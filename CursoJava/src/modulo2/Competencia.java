package modulo2;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Competencia {

	private JFrame frame;
	private JTextField textPuesto;
	private JTextField textPremio;
	private JButton btnPremio;
	private JButton btnLimpiar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Competencia window = new Competencia();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Competencia() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblCompetencia = new JLabel("Competencia");
		lblCompetencia.setFont(new Font("Tahoma", Font.PLAIN, 21));
		lblCompetencia.setBounds(177, 22, 162, 31);
		frame.getContentPane().add(lblCompetencia);
		
		textPuesto = new JTextField();
		textPuesto.setBounds(66, 90, 86, 20);
		frame.getContentPane().add(textPuesto);
		textPuesto.setColumns(10);
		
		JLabel lblPuesto = new JLabel("Puesto");
		lblPuesto.setBounds(75, 65, 46, 14);
		frame.getContentPane().add(lblPuesto);
		
		textPremio = new JTextField();
		textPremio.setBounds(237, 90, 86, 20);
		frame.getContentPane().add(textPremio);
		textPremio.setColumns(10);
		
		JLabel lblPremio = new JLabel("Premio");
		lblPremio.setBounds(237, 64, 46, 14);
		frame.getContentPane().add(lblPremio);
		
		btnPremio = new JButton("Premio");
		btnPremio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				int iPuesto = Integer.parseInt(textPuesto.getText());
				switch (iPuesto) {
				case 1: textPremio.setText("Oro");
					break;
				case 2: textPremio.setText("Plata");
					break;
				case 3: textPremio.setText("Bronce");
					break;

				default:
					textPremio.setText("Nada");
					break;
				}
			}
		});
		btnPremio.setBounds(66, 157, 89, 23);
		frame.getContentPane().add(btnPremio);
		
		btnLimpiar = new JButton("Limpiar");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				 textPremio.setText("");
				 textPuesto.setText("");
			}
		});
		btnLimpiar.setBounds(249, 157, 89, 23);
		frame.getContentPane().add(btnLimpiar);
	}
	public JButton getBtnPremio() {
		return btnPremio;
	}
	public JButton getBtnLimpiar() {
		return btnLimpiar;
	}
	public JTextField getTextPuesto() {
		return textPuesto;
	}
	public JTextField getTextPremio() {
		return textPremio;
	}
}
