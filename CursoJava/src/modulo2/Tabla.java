package modulo2;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextPane;
import javax.swing.JTextField;

public class Tabla {

	private JFrame frame;
	private JTextField textTabla;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Tabla window = new Tabla();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Tabla() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 549, 609);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblMultiplos = new JLabel("Multiplos");
		lblMultiplos.setBounds(197, 38, 46, 14);
		frame.getContentPane().add(lblMultiplos);
		
		JTextPane textPane = new JTextPane();
		textPane.setBounds(93, 134, 214, 298);
		frame.getContentPane().add(textPane);
		
		textTabla = new JTextField();
		textTabla.setBounds(376, 92, 86, 20);
		frame.getContentPane().add(textTabla);
		textTabla.setColumns(10);
		
		JLabel lblLaTablaDel = new JLabel("La tabla del:");
		lblLaTablaDel.setBounds(393, 67, 154, 14);
		frame.getContentPane().add(lblLaTablaDel);
	}
}
