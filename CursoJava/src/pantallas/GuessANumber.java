package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GuessANumber {

	private JFrame frame;
	private JTextField textNumeroElegido;
	private JLabel lblCantintentos;
	private JLabel lblElegido;
	private JButton btnAceptar;
	private JLabel lblArribaAbajo;
	private JLabel lblTextoArribaAbajo;
	private int cantidadIntentos;
	private JLabel lblNewLabel_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GuessANumber window = new GuessANumber();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GuessANumber() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Elija un Numero");
		lblNewLabel.setBounds(75, 21, 222, 14);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblElijaUnNumero = new JLabel("Elija un numero entre 1 y 99");
		lblElijaUnNumero.setBounds(39, 124, 158, 14);
		frame.getContentPane().add(lblElijaUnNumero);
		
		lblNewLabel_1 = new JLabel("El elegido es: ");
		lblNewLabel_1.setVisible(false);
		lblNewLabel_1.setBounds(243, 84, 89, 14);
		frame.getContentPane().add(lblNewLabel_1);
		
		textNumeroElegido = new JTextField();
		textNumeroElegido.setBounds(231, 121, 86, 20);
		frame.getContentPane().add(textNumeroElegido);
		textNumeroElegido.setColumns(10);
		
		lblArribaAbajo = new JLabel("");
		lblArribaAbajo.setVisible(false);
		lblArribaAbajo.setIcon(new ImageIcon(GuessANumber.class.getResource("/iconos/dedoArriba_32px.png")));
		lblArribaAbajo.setBounds(75, 159, 86, 56);
		frame.getContentPane().add(lblArribaAbajo);
		
		lblTextoArribaAbajo = new JLabel("Mas Alto");
		lblTextoArribaAbajo.setVisible(false);
		lblTextoArribaAbajo.setBackground(Color.PINK);
		lblTextoArribaAbajo.setOpaque(true);
		lblTextoArribaAbajo.setBounds(39, 226, 100, 14);
		frame.getContentPane().add(lblTextoArribaAbajo);
		
		btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int imiValor = Integer.parseInt(textNumeroElegido.getText());
				int ivalorCompu = Integer.parseInt(lblElegido.getText());
				if (imiValor>ivalorCompu){
					
					lblTextoArribaAbajo.setVisible(true);
					lblTextoArribaAbajo.setText("Mas Abajo");	
					
					lblArribaAbajo.setVisible(true);
					lblArribaAbajo.setIcon(new ImageIcon(GuessANumber.class.getResource("/iconos/dedoAbajo_32px.png")));
				}
				else if (imiValor<ivalorCompu){
					
					lblTextoArribaAbajo.setVisible(true);
					lblTextoArribaAbajo.setText("Mas Arriba");	
					lblArribaAbajo.setVisible(true);
					lblArribaAbajo.setIcon(new ImageIcon(GuessANumber.class.getResource("/iconos/dedoArriba_32px.png")));
					
				}else {
					
					lblTextoArribaAbajo.setVisible(true);
					lblTextoArribaAbajo.setText("Vamo Carajo");	
					
					lblNewLabel_1.setVisible(true);
					lblElegido.setVisible(true);
					
					lblArribaAbajo.setVisible(true);
					lblArribaAbajo.setIcon(new ImageIcon(GuessANumber.class.getResource("/iconos/sol_32px.png")));
					}
			    cantidadIntentos ++;
				lblCantintentos.setText(Integer.toString(cantidadIntentos));

			}
		});
		btnAceptar.setBounds(335, 120, 89, 23);
		frame.getContentPane().add(btnAceptar);
		
		lblElegido = new JLabel("");
		lblElegido.setVisible(false);
		lblElegido.setBackground(Color.GREEN);
		lblElegido.setOpaque(true);
		lblElegido.setBounds(341, 78, 67, 20);
		
		int iNumElegido = (int) (Math.random()*1000%99)+1;
		lblElegido.setText(Integer.toString(iNumElegido));
		
		frame.getContentPane().add(lblElegido);
		
		JLabel lblCantidadDeIntentos = new JLabel("Cantidad de Intentos");
		lblCantidadDeIntentos.setBounds(26, 66, 119, 14);
		frame.getContentPane().add(lblCantidadDeIntentos);
		
		lblCantintentos = new JLabel("");
		lblCantintentos.setBounds(164, 66, 46, 14);
		frame.getContentPane().add(lblCantintentos);
		
		JButton btnLimpiar = new JButton("Limpiar");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cantidadIntentos=0;
				lblTextoArribaAbajo.setVisible(false);	
				lblArribaAbajo.setVisible(false);
				lblNewLabel_1.setVisible(false);
				lblElegido.setVisible(false);
				
				
				}
		});
		btnLimpiar.setBounds(277, 222, 89, 23);
		frame.getContentPane().add(btnLimpiar);
		
		
		
	}

	public JButton getBtnAceptar() {
		return btnAceptar;
	}
	public JLabel getLblArribaAbajo() {
		return lblArribaAbajo;
	}
}
