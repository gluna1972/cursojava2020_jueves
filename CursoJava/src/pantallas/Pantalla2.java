package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Pantalla2 {

	private JFrame frame;
	private JLabel lblByteMin;
	private JLabel lblBiteMax;
	private JTextField txtTiposDeDatos;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pantalla2 window = new Pantalla2();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Pantalla2() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 680, 436);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		

		
		JLabel lblTitulo = new JLabel("Tipo de datos");
		lblTitulo.setForeground(new Color(0, 0, 0));
		lblTitulo.setBackground(new Color(255, 215, 0));
		lblTitulo.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblTitulo.setBounds(59, 108, 146, 43);
		frame.getContentPane().add(lblTitulo);
		
		txtTiposDeDatos = new JTextField();
		txtTiposDeDatos.setFont(new Font("Tahoma", Font.PLAIN, 37));
		txtTiposDeDatos.setText("Tipos de datos");
		txtTiposDeDatos.setBounds(200, 17, 316, 51);
		frame.getContentPane().add(txtTiposDeDatos);
		txtTiposDeDatos.setColumns(10);
		
		JLabel lblMaximos = new JLabel("Maximos");
		lblMaximos.setForeground(new Color(0, 0, 0));
		lblMaximos.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblMaximos.setBackground(new Color(255, 215, 0));
		lblMaximos.setBounds(261, 108, 146, 43);
		frame.getContentPane().add(lblMaximos);
		
		JLabel lblMinimos = new JLabel("Minimos");
		lblMinimos.setForeground(Color.BLACK);
		lblMinimos.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblMinimos.setBackground(new Color(255, 215, 0));
		lblMinimos.setBounds(463, 108, 146, 43);
		frame.getContentPane().add(lblMinimos);
		
		JLabel lblByte = new JLabel("byte");
		lblByte.setBounds(80, 162, 46, 14);
		frame.getContentPane().add(lblByte);
		
		lblByteMin = new JLabel("");
		lblByteMin.setFont(new Font("Arial Black", Font.BOLD | Font.ITALIC, 11));
		lblByteMin.setBackground(new Color(175, 238, 238));
		lblByteMin.setOpaque(true);
		lblByteMin.setBounds(470, 162, 46, 14);
		frame.getContentPane().add(lblByteMin);
		
		lblBiteMax = new JLabel("");
		lblBiteMax.setOpaque(true);
		lblBiteMax.setBackground(new Color(175, 238, 238));
		lblBiteMax.setBounds(271, 162, 46, 14);
		frame.getContentPane().add(lblBiteMax);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setToolTipText("I'm the best!!!");
		lblNewLabel.setIcon(new ImageIcon(Pantalla2.class.getResource("/iconos/medalla_oro_64px.png")));
		lblNewLabel.setBounds(529, 0, 64, 81);
		frame.getContentPane().add(lblNewLabel);
		
		JButton btnCalcular = new JButton("Calcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				byte bMin=Byte.MIN_VALUE;
				byte bMax=Byte.MIN_VALUE;
				lblByteMin.setText(bMin);
				lblBiteMax.setText(bMax);
			}
		});
		btnCalcular.setBounds(70, 333, 89, 23);
		frame.getContentPane().add(btnCalcular);
	}
}
