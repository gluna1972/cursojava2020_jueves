package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import java.awt.Font;
import java.awt.Color;
import javax.swing.SwingConstants;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Pantalla3 {

	private JFrame frame;
	private JTextField textNota1;
	private JTextField textNota2;
	private JTextField textNota3;
	private JLabel lblResult;
	private JButton btnLimpiar;
	private JLabel lblImagen;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pantalla3 window = new Pantalla3();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Pantalla3() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 762, 708);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblPromedio = new JLabel("Promedio de Notas");
		lblPromedio.setIcon(new ImageIcon(Pantalla3.class.getResource("/javax/swing/plaf/basic/icons/image-delayed.png")));
		lblPromedio.setVerticalAlignment(SwingConstants.TOP);
		lblPromedio.setForeground(Color.RED);
		lblPromedio.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblPromedio.setBounds(216, 37, 265, 50);
		frame.getContentPane().add(lblPromedio);
		
		
		textNota1 = new JTextField();
		textNota1.setBounds(195, 204, 116, 22);
		frame.getContentPane().add(textNota1);
		textNota1.setColumns(10);
		
		
		textNota2 = new JTextField();
		textNota2.setBounds(195, 247, 116, 22);
		frame.getContentPane().add(textNota2);
		textNota2.setColumns(10);
		
		textNota3 = new JTextField();
		textNota3.setBounds(195, 292, 116, 22);
		frame.getContentPane().add(textNota3);
		textNota3.setColumns(10);
		
		JLabel lblNota = new JLabel("Nota1");
		lblNota.setBounds(57, 207, 56, 16);
		frame.getContentPane().add(lblNota);
		
		JLabel lblNota_1 = new JLabel("Nota2");
		lblNota_1.setBounds(57, 250, 56, 16);
		frame.getContentPane().add(lblNota_1);
		
		JLabel lblNota_2 = new JLabel("Nota13");
		lblNota_2.setBounds(57, 295, 56, 16);
		frame.getContentPane().add(lblNota_2);
		
		lblResult = new JLabel("");
		lblResult.setBackground(Color.CYAN);
		lblResult.setOpaque(true);
		lblResult.setBounds(551, 274, 70, 25);
		frame.getContentPane().add(lblResult);
		
		btnLimpiar = new JButton("Limpiar");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textNota1.setText("");
				textNota2.setText("");
				textNota3.setText("");
				
				lblResult.setText("");
				lblResult.setBackground(Color.cyan);
				lblImagen.setIcon(new ImageIcon(Pantalla3.class.getResource("/javax/swing/plaf/basic/icons/JavaCup16.png")));
			}
		});
		btnLimpiar.setBounds(609, 338, 97, 25);
		frame.getContentPane().add(btnLimpiar);
		
		lblImagen = new JLabel("");
		lblImagen.setIcon(new ImageIcon(Pantalla3.class.getResource("/javax/swing/plaf/basic/icons/JavaCup16.png")));
		lblImagen.setBounds(576, 204, 83, 65);
		frame.getContentPane().add(lblImagen);
		
		JButton btnCalcular = new JButton("Calcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				float fnota1 = Float.parseFloat(textNota1.getText());
				float fnota2 = Float.parseFloat(textNota2.getText());
				float fnota3 = Float.parseFloat(textNota3.getText());
				
				float fpromedio = (fnota1+fnota2+fnota3)/3;
				lblResult.setText(Float.toString(fpromedio));
				
				if (fpromedio >=7){
					
					lblImagen.setIcon(new ImageIcon(Pantalla3.class.getResource("/iconos/dedoArriba_32px.png")));
					lblResult.setBackground(Color.GREEN);
				}
				else{
					
					lblImagen.setIcon(new ImageIcon(Pantalla3.class.getResource("/iconos/fallido2_32px.png")));
					lblResult.setBackground(Color.red);
				}
					
				
				
			}
		});
		btnCalcular.setBounds(482, 338, 97, 25);
		frame.getContentPane().add(btnCalcular);
	}
	public JTextField getTextNota1() {
		return textNota1;
	}
	public JButton getBtnLimpiar() {
		return btnLimpiar;
	}
}
